;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                             ;;
;;;        A diphone voice for Telugu language                                  ;;
;;;                                                                             ;;
;;;  Copyright (c) 2005, DONLab, Dept. of CS&E,                                 ;; 
;;;                      IIT Madras <speech@lantana.tenet.res.in>               ;; 
;;;  Copyright (c) 2005, Chaitanya Kamisetty <chaitanya@atc.tcs.co.in>          ;;
;;;                                                                             ;;
;;;  This program is a part of festival-te.					;;
;;;  										;;
;;;  festival-te is free software; you can redistribute it and/or modify        ;;
;;;  it under the terms of the GNU General Public License as published by	;;
;;;  the Free Software Foundation; either version 2 of the License, or		;;
;;;  (at your option) any later version.					;;
;;;										;;
;;;  This program is distributed in the hope that it will be useful,		;;
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of		;;
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		;;
;;;  GNU General Public License for more details.				;;
;;;										;;
;;;  You should have received a copy of the GNU General Public License		;;
;;;  along with this program; if not, write to the Free Software		;;
;;;  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA  ;;
;;;										;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; Try to find out where we are
(if (assoc 'telugu_NSK_diphone voice-locations)
    (defvar telugu_dir 
      (cdr (assoc 'telugu_NSK_diphone voice-locations)))
    ;;; Not installed in Festival yet so assume running in place
    (defvar telugu_dir (pwd)))

(if (not (probe_file (path-append telugu_dir "festvox/")))
    (begin
     (format stderr "telugu_NSK_diphone Can't find voice scm files they are not in\n")
     (format stderr "   %s\n" (path-append telugu_dir "festvox/"))
     (format stderr "   Either the voice isn't linked into Festival\n")
     (format stderr "   or you are starting festival in the wrong directory\n")
     (error)))

;;;  Add the directory contains general voice stuff to load-path
(set! load-path (cons (path-append telugu_dir "festvox/") load-path))
;;;  Path for common telugu modules
;;; Debian distribution defines datadir, others use libdir
(if (symbol-bound? 'datadir)
	(set! load-path (cons (path-append datadir "telugu_scm/") load-path)))
(set! load-path (cons (path-append libdir "telugu_scm/") load-path))

;;; other files we need
(require 'telugu_phones)
(require 'telugu_lex)
(require 'telugu_token)
(require 'telugu_NSK_int)
(require 'telugu_NSK_dur)
(require 'telugu_NSK_ene)

;;;  Ensure we have a festival with the right diphone support compiled in
(require_module 'UniSyn)

(set! telugu_lpc_sep 
     (list
      '(name "telugu_lpc_sep")
      (list 'index_file (path-append telugu_dir "dic/NSKdiph.est"))
      '(grouped "false")
      (list 'coef_dir (path-append telugu_dir "lpc"))
      (list 'sig_dir  (path-append telugu_dir "lpc"))
      '(coef_ext ".lpc")
      '(sig_ext ".res")
      (list 'default_diphone 
	     (string-append
	      (car (cadr (car (PhoneSet.description '(silences)))))
	      "-"
	      (car (cadr (car (PhoneSet.description '(silences)))))))))

(set! telugu_lpc_group 
     (list
      '(name "NSK_lpc_group")
      (list 'index_file 
	     (path-append telugu_dir "group/NSKlpc.group"))
      '(grouped "true")
      (list 'default_diphone 
	     (string-append
	      (car (cadr (car (PhoneSet.description '(silences)))))
	      "-"
	      (car (cadr (car (PhoneSet.description '(silences)))))))))

;;Go ahead and set up the diphone db
;(set! telugu_db_name (us_diphone_init telugu_lpc_sep))
;;Once you've built the group file you can comment out the above and
;;uncomment the following.
(set! telugu_db_name (us_diphone_init telugu_lpc_group))


(define (telugu_NSK_diphone_fix utt)
"(telugu_NSK_diphone_fix UTT)
Map phones to phonological variants if the diphone database supports
them."
  (mapcar
   (lambda (s)
     (let ((name (item.name s)))
       ;; Check and do something maybe 
       ))
   (utt.relation.items utt 'Segment))
  utt)

(define (telugu_voice_reset)
  "(telugu_voice_reset)
Reset global variables back to previous voice."
  ;; whatever
)


(define (telugu_NSK_diphone_fix_phone_name utt seg)
"(telugu_fix_phone_name UTT SEG)
Add the feature diphone_phone_name to given segment with the appropriate
name for constructing a diphone.  Basically adds _ if either side is part
of the same consonant cluster, adds $ either side if in different
syllable for preceding/succeeding vowel syllable."
  (let ((name (item.name seg)))
    (cond
     ((string-equal name "pau") t)
     ((string-equal "-" (item.feat seg 'ph_vc))
      (if (and (member_string name '(r w y l))
	       (member_string (item.feat seg "p.name") '(p t k b d g))
	       (item.relation.prev seg "SylStructure"))
	  (item.set_feat seg "us_diphone_right" (format nil "_%s" name)))
      (if (and (member_string name '(w y l m n p t k))
	       (string-equal (item.feat seg "p.name") 's)
	       (item.relation.prev seg "SylStructure"))
	  (item.set_feat seg "us_diphone_right" (format nil "_%s" name)))
      (if (and (string-equal name 's)
	       (member_string (item.feat seg "n.name") '(w y l m n p t k))
	       (item.relation.next seg "SylStructure"))
	  (item.set_feat seg "us_diphone_left" (format nil "%s_" name)))
      (if (and (string-equal name 'hh)
	       (string-equal (item.feat seg "n.name") 'y))
	  (item.set_feat seg "us_diphone_left" (format nil "%s_" name)))
      (if (and (string-equal name 'y)
	       (string-equal (item.feat seg "p.name") 'hh))
	  (item.set_feat seg "us_diphone_right" (format nil "_%s" name)))
      (if (and (member_string name '(p t k b d g))
	       (member_string (item.feat seg "n.name") '(r w y l))
	       (item.relation.next seg "SylStructure"))
	  (item.set_feat seg "us_diphone_left" (format nil "%s_" name)))
      )
     ((string-equal "ah" (item.name seg))
      (item.set_feat seg "us_diphone" "aa"))

   )))


;;;  Full voice definition 
(define (voice_telugu_NSK_diphone)
"(voice_telugu_NSK_diphone)
Set speaker to NSK in telugu from DON."
  (voice_reset)
  (Parameter.set 'Language 'telugu)
  ;; Phone set
  (Parameter.set 'PhoneSet 'telugu)
  (PhoneSet.select 'telugu)

  ;; token expansion (numbers, symbols, compounds etc)
  (Parameter.set 'Token_Method 'Token_Any)
  (set! token_to_words telugu_token_to_words)

  ;; No pos prediction (get it from lexicon)
  (set! pos_lex_name nil)
  (set! guess_pos telugu_guess_pos) 
  ;; Phrase break prediction by punctuation
  (set! pos_supported nil) ;; well not real pos anyhow
  ;; Phrasing
  (set! phrase_cart_tree telugu_phrase_cart_tree_2)
  (Parameter.set 'Phrase_Method 'cart_tree)
  ;; Lexicon selection
  (lex.select "telugu")

  ;; No postlexical rules
  (set! postlex_rules_hooks nil)

  ;; Accent and tone prediction 
  
  (set! int_accent_cart_tree telugu_accent_cart_tree)
  (Parameter.set 'Int_Target_Method 'Simple) 

  ;; Duration prediction
  (set! duration_cart_tree telugu_NSK::zdur_tree) 

  ;;(set! duration_ph_info telugu_NSK::phone_data)
  (set! duration_ph_info telugu_NSK::phone_durs)   
  (Parameter.set 'Duration_Method 'Tree_ZScores)
  (Parameter.set 'Duration_Stretch 1.4)

  
  ;; Energy prediction
  (set! energy_cart_tree telugu_NSK::zene_treeMAX2) 
  (set! energy_ph_info telugu_NSK::phone_enesMAX2)
  (Parameter.set 'Energy_Method 'Tree_ZScores)

  ;; Waveform synthesizer: diphones
  (set! UniSyn_module_hooks (list telugu_NSK_diphone_fix))
  (set! us_abs_offset 0.0)
  (set! window_factor 1.0)
  (set! us_rel_offset 0.0)
  (set! us_gain 0.9)

  (Parameter.set 'Synth_Method 'UniSyn)
  (Parameter.set 'us_sigpr 'lpc)
  (us_db_select telugu_db_name)


  ;; set callback to restore some original values changed by this voice
  (set! current_voice_reset telugu_voice_reset)

  (set! current-voice 'telugu_NSK_diphone)
)

(proclaim_voice

 'telugu_NSK_diphone
 '((language telugu)
   (gender male)
   (dialect COMMENT)
   (description
    "COMMENT"
    )
   (builtwith festvox-1.2)
   (coding UTF-8)
   ))

(provide 'telugu_NSK_diphone)
