Telugu Festival Text-to-Speech System
=====================================

festival-te package provides the modules required to synthesize speech
in Telugu language using the Festival text-to-speech synthesizer. Given
text represented in Unicode (UTF-8 encoding), it uses the festival synthesizer
to generate speech in Telugu.

It is possible to use screen-readers like gnopernicus on GNOME desktop to read
out desktop items, menus, documents etc. in Telugu. Refer to the section 
'Enabling gnopernicus for Telugu' in this document for instructions.

Refer to the file HACKING for technical details on building festival TTS for Telugu. 

License
=======

This package is licensed under GNU General Public License. See the file COPYING
for details.

Installation
============

Refer to the file INSTALL for installation instructions.

Using Telugu Festival
=====================

After installing festival-te, it can be used to read out Telugu Unicode text 
using the regular festival functions. Sample text files can be found in 
examples/ directory. Some example methods of using the TTS are as given below:

1. Using Festival command line
   Set current voice in festival to the Telugu voice and read out the text.

	$ festival
	festival> (voice_telugu_NSK_diphone)
	festival> (tts "examples/sample.txt" nil)

2. Running Festival in batch mode using the command line

	$ cd examples/
	$ festival -b example_run.scm


Enabling festival language option for Telugu
============================================
To use festival --language option, and (voice_telugu) method, languages.scm.patch needs
to be applied. The patch can be applied using the following commands:

	$ su
	# patch /usr/share/festival/languages.scm patches/language.scm.patch

If festival is installed in a location different from "/usr/share/festival", use that path as the
first argument to 'patch' command.

After the patch is applied, festival language option can be used as follows:

1. As an argument to festival when invoked from command line
	
	$ festival --language telugu --tts examples/sample.txt

2. Setting language to telugu using festival command line (sets current-voice to default telugu voice).

	$ festival
	festival> (voice_telugu)

If this patch is not applied, telugu voice can still be used by calling (voice_telugu_NSK_diphone) at
the festival command line.

Enabling Gnopernicus Screen Reader with festival-te
===================================================
In order to use gnopernicus screen reader with festival-te, you need the gnome-speech library
(> gnome-speech-0.4.0) You can patch the older version (0.3.8) using patches/festivalsynthesisdriver.c-0.3.8.patch

For detailed instructions on setting up your system to read out Telugu desktop, refer to the following:
http://telugu.sarovar.org/wiki/index.php/SpeechAssistance

